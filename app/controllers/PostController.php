<?php
/**
 * 
 * PostController
 * 
 */
class PostController extends \BaseController
{
    
    protected $layout = 'empty_layout';
    
    
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index() 
    {
        $post = new Post();
        $items = $post->where('is_active', '=', 1)->get();
        
        return Response::json($items);
    }

    /**
     * Validate input data
     * 
     * @param  array $data - input data for validating.
     * @return boolean
     */
    protected function isValidSaveData(array $data) 
    {
        $rules = [
            'title' => 'required|min:5',
            'summary' => 'required',
            'content' => 'required',
            'is_active' => 'required|boolean'
        ];
        $validator = Validator::make($data, $rules);
        return $validator->fails();
    }


    public function store() 
    {

        if ($this->isValidSaveData(Input::all())) {
            return Response::make('Invalid data', 400);
        }
            
        $data = [
            'title' => Request::get('title'),
            'summary' => Request::get('summary'),
            'content' => Request::get('content'),
            'is_active' => (bool)Request::get('is_active')
        ];

        $post = new Post();
        foreach ($data as $k => $v) {
            $post->{$k} = $v;
        }

        try {
            $post->save();
        } catch(Exception $e) {
            /**
            * @todo Log exception 
            **/
            return 'Bad input data';
        }
            
        return Response::make('Created', 201);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create() 
    {
        return 'OK ';
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id) 
    {
        $post = new Post();
        $data = $post->where('is_active', '=', 1)->find(abs($id));
        if ($data) {
            return Response::json($data);
        }
        return Response::make('Not found', 404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id) 
    {
        $post = new Post();
        $data = $post->where('is_active', '=', 1)->find(abs($id));
        
        if ($data) {
            return Response::json($data);
        }
        return Response::make('Not found', 404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id) 
    {
        if ($this->isValidSaveData(Input::all())) {
            return Response::make('Invalid data', 400);
        }
        
        $data = [
            'title' => Request::get('title'),
            'summary' => Request::get('summary'),
            'content' => Request::get('content'),
            'is_active' => (bool)Request::get('is_active')
        ];

        $post = Post::find($id);
        foreach ($data as $k => $v) {
            $post->{$k} = $v;
        }

        try {
            $post->save();
        } catch(Exception $e) {
            Log::error($e);
            return Response::make('Invalid data', 403);
        }
        
        return Response::make('Record updated', 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  integer $id
     * @return Response 
     */
    public function destroy($id) 
    {
        $post = Post::find($id);
        
        if (!$post) {
            return Response::make('Resource not found!', 403);
        }
        
        return Response::make($post->delete() ? 'OK' : '', 200);
    }

}
