<?php
/**
 * PostTest
 * Unit tests
 */
class PostTest extends TestCase {

    protected $_target_url = '/posts';
    
    protected function getFirstActiveItem() {
        $post = new Post();
        $item = $post->where('is_active', 1)->first();
        return $item && is_object($item) ? $item : false;
    }
    
    public function testIndex() {
        
        $this->client->request('GET', $this->_target_url);

        $response = $this->client->getResponse();
        $this->assertTrue($this->isJsonString($response->getContent()));
        $this->assertTrue($this->client->getResponse()->isOk());
        $this->assertNotEmpty($this->client->getResponse());
        
    }

    
    public function testShow() {
        $this->client->request('GET', $this->_target_url.'/1');
        $response = $this->client->getResponse();
        $this->assertContains($response->getStatusCode(), [200, 404]);
        /** check with valid db entry **/
        if ($response->getStatusCode() == 404) {
            $item = $this->getFirstActiveItem();
            if ($item !== false) {
                $this->client->request('GET', $this->_target_url.'/'.$item->id);
                $response = $this->client->getResponse();
                $this->assertEquals(200, $response->getStatusCode());
                
                /** check invalid record **/
                $this->client->request('GET', $this->_target_url.'/0');
                $response = $this->client->getResponse();
                $this->assertNotEquals(200, $response->getStatusCode());
            }
        }
    }
    
    
    public function testCreate() {
        $now = date('Y-m-d H:i:s');
        $post_data = [
            'title' => 'New record at '.$now,
            'summary' => 'Summary '.$now,
            'content' => 'New content at '.$now,
            'is_active' => 1
        ];
        
        /** Open form **/
        $this->client->request('GET', $this->_target_url.'/create');
        $response = $this->client->getResponse();
        $this->assertTrue( $response->isOk() );
        
        $this->client->request('POST', $this->_target_url, $post_data);
        $response = $this->client->getResponse();
        $this->assertEquals(201, $response->getStatusCode());
        
        /* test invalid input data */
        unset($post_data['title']);
        $this->client->request('POST', $this->_target_url, $post_data);
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());
    }
    
        
    public function testDelete() {
        $this->client->request('DELETE', $this->_target_url.'/9' );
        $response = $this->client->getResponse();
        $this->assertContains($response->getStatusCode(), [200, 403]);
        if ($response->getStatusCode() == 403) {
            $item = $this->getFirstActiveItem();
            if ($item !== false) {
                $this->client->request('DELETE', $this->_target_url.'/'.$item->id );
                $response = $this->client->getResponse();
                $this->assertEquals(200, $response->getStatusCode());
            }
        }
    }
    
    
    public function testUpdate() {
        $now = date('Y-m-d H:i:s');
        $update_data = [
            'title' => 'Record upddated at ' . $now,
            'summary' => 'Summary updated at ' . $now,
            'content' => 'Content updated at ' . $now,
            'is_active' => 1
        ];
        $item = $this->getFirstActiveItem();
        if ( $item !== false ) {
            $this->client->request('GET', $this->_target_url.'/'.$item->id.'/edit');
            $response = $this->client->getResponse();
            $this->assertTrue($response->isOk());
            $this->assertTrue($this->isJsonString($response->getContent()));
            $loaded_item = json_decode($response->getContent());
            $this->assertEquals($loaded_item->id, $item->id);
            
            $this->client->request('PUT', $this->_target_url.'/'.$item->id, $update_data);
            $response = $this->client->getResponse();
            $this->assertEquals(200, $response->getStatusCode());
            
            /** test invalid data **/
            unset($update_data['title']);
            $this->client->request('PUT', $this->_target_url.'/'.$item->id, $update_data);
            $response = $this->client->getResponse();
            $this->assertNotEquals(200, $response->getStatusCode());
            
            /** test load ivlaid data **/
            $this->client->request('GET', $this->_target_url.'/0/edit');
            $response = $this->client->getResponse();
            $this->assertEquals(404, $response->getStatusCode());
        }
    }
    
    
    public function isJsonString($content) {
        json_decode($content);
        return (json_last_error() == JSON_ERROR_NONE);
    }
}
