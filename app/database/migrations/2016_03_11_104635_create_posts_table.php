<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        \Illuminate\Support\Facades\Schema::create('posts', function($table) {
            $table->increments('id');
            $table->string('title');
            $table->text('summary');
            $table->longText('content');
            $table->boolean('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        \Illuminate\Support\Facades\Schema::drop('posts');
    }

}
