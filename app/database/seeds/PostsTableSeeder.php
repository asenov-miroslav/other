<?php

use Faker\Factory as Faker;

class PostsTableSeeder extends Seeder {

    public function run() {
        $faker = Faker::create();
        foreach (range(1, 50) as $index) {
            $post = new Post();
            $data = array(
                'title' => $faker->sentence(1),
                'summary' => $faker->text(200),
                'content' => $faker->paragraph(4),
                'is_active' => 1
            );

            foreach ($data as $k => $v) {
                $post->{$k} = $v;
            }
            $post->save();
        }
    }

}
